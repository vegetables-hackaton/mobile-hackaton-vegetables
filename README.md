## Hackathon - Entering to rooms  

<hr>

### Commits:
#### Commit types

| **Emoji** | **Commit type** |
|:---------:|:---------------:|
|    🔥     |  Major update   |
|    💧     |  Minor update   |
|    👾     |       Fix       |
|    🐣     |       Add       |
|     ✂     |    Refactor     |
|    🥂     |     Finish      |