package com.example.myapplication

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction

class RegisterFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val newView =  inflater.inflate(R.layout.fragment_register, container, false)
        val btnRegister = newView.findViewById<Button>(R.id.btn_register)
        btnRegister.setOnClickListener {
            val newFragment = BlankFragment()
            val transaction = childFragmentManager.beginTransaction()
            transaction.replace(R.id.registerLayout, BlankFragment())
            transaction.addToBackStack(null)
            transaction.commit()
        }

        return newView
    }


}
